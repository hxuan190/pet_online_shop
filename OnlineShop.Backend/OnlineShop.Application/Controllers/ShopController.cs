﻿using Microsoft.AspNetCore.Mvc;

namespace OnlineShop.Application.Controllers
{
    public class ShopController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
    }
}
