﻿
using OnlineShop.Core;
using OnlineShop.Core.Schemas;
using OnlineShop.Core.Schemas.Base;
using OnlineShop.Services.Base;

namespace OnlineShop.Services
{
	public interface IProduct : IBaseService<ProductSchema>
	{

	}



	public class ProductService : BaseService<ProductSchema, DataContext>, IProduct
	{

		private readonly DataContext context;

		public ProductService(DataContext _ctx) : base(_ctx)
		{
			this.context = _ctx;
		}

	}
}
